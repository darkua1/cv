# Hello

I'm a Portuguese programmer, living in Berlin, with +10 years of professional experience developing software, mostly in startup environments.

I'm fluent in C/C++, Javascript( Node/React/Vue),Erlang-Elixir, Golang, and i build code following design patterns for both OO and Functional Paradigms. and also fully capable of doing operations and deployment for micro-service / serverless architectures that can scale horizontally with docker+ k8s on GKE or AWS.

I worked with many hats already, Frontend, Backend, Full-Stack, Team Lead, DevOps, Founder… what unlock my favorite hat, the Architect since my strongest/experience skill is to plan, design, and oversee the construction of Web/Mobile Applications.

For more detail experience check my [`cv`](../master/sergio_cv_2020.pdf)

Im always happy to discuss any challenging project, feel free to book a intro-meeting in my [`calendar`](https://calendly.com/sergioveiga/meeting)

Have a nice day

> "When not looking, waves of possibility, when looking, particles of experience" Quantum Phisics
